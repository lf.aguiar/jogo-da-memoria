const mensagens = {
  sucesso: {
    texto: 'Combinação correta',
    estilo: 'alert-success',
  },
  falha: {
    texto: 'Combinação errada',
    estilo: 'alert-danger',
  },
};

const util = Util;

class Tela {
  static capturarDivConteudoPrincipal() {
    return document.getElementById('conteudo-principal');
  }

  static criarCardHeroiHtml(item) {
    return `<div class="col-md-3">
    <br>
    <div class="card" style="width: 50%;" onclick="window.verificarSelecaoCard('${item.id}', '${item.nome}')">
      <img name="${item.nome}"
        src="./files/imgs/${item.src}"
        class="card-img-top"
        alt="${item.alt}"
      />
    </div>
    <br>
  </div>`;
  }

  static criarTodosCardsHeroiHtml(itens) {
    return itens.map(this.criarCardHeroiHtml).join('');
  }

  static exibirHeroisCorretos(nomeHeroi, img) {
    const heroisPorNome = document.getElementsByName(nomeHeroi);

    heroisPorNome.forEach((heroi) => (heroi.src = `./files/imgs/${img}`));
  }

  static inserirCardsHtmlNaTela(arquivosHerois) {
    const todosCardsHeroiHtml = this.criarTodosCardsHeroiHtml(arquivosHerois);
    this.capturarDivConteudoPrincipal().innerHTML = todosCardsHeroiHtml;
  }

  static configurarBotaoJogar(funcaoOnclickCustom) {
    const btnJogar = document.getElementById('btn-jogar');
    btnJogar.onclick = funcaoOnclickCustom;
  }

  static configurarBotaoSelecionarCard(funcaoOnclickCustom) {
    window.verificarSelecaoCard = funcaoOnclickCustom;
  }

  static configurarBotaoMostrarTudo(funcaoOnclickCustom) {
    const btnMostratTudo = document.getElementById('btn-mostrar-tudo');
    btnMostratTudo.onclick = funcaoOnclickCustom;
  }

  static async exibirMensagem(sucesso = true) {
    const elemento = document.getElementById('mensagem');

    if (sucesso) {
      elemento.classList.remove(mensagens.falha.estilo);
      elemento.classList.add(mensagens.sucesso.estilo);
      elemento.innerHTML = mensagens.sucesso.texto;
    } else {
      elemento.classList.remove(mensagens.sucesso.estilo);
      elemento.classList.add(mensagens.falha.estilo);
      elemento.innerHTML = mensagens.falha.texto;
    }

    elemento.classList.remove('invisible');
    await util.timeout(1000);
    elemento.classList.add('invisible');
  }

  static exibirCarregando(mostrar = true) {
    const carregando = document.getElementById('carregando');
    if (mostrar) {
      carregando.classList.remove('invisible');
      return;
    }
    carregando.classList.add('invisible');
  }

  static iniciarContador() {
    let contarAte = 3;
    const elementoContator = document.getElementById('contador');
    const identificadorNoTexto = '$$contador';
    const textoPadrao = `Começando em ${identificadorNoTexto} segundos...`;

    const atualizarTexto = () =>
      (elementoContator.innerHTML = textoPadrao.replace(
        identificadorNoTexto,
        contarAte--
      ));

    atualizarTexto();

    const idDoIntervalo = setInterval(atualizarTexto, 1000);
    return idDoIntervalo;
  }

  static limparContador(idDoIntervalo) {
    clearInterval(idDoIntervalo);
    document.getElementById('contador').innerHTML = '';
  }
}
