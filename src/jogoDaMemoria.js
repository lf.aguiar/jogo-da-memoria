class JogoDaMemoria {
  constructor({ tela, util }) {
    this.tela = tela;
    this.util = util;

    this.imagensHeroisIniciais = [
      {
        id: 'cap',
        nome: 'cap',
        src: 'cap.png',
        alt: 'Capitão América',
      },
      {
        id: 'dare',
        nome: 'dare',
        src: 'dare.png',
        alt: 'Demolidor',
      },
      {
        id: 'flash',
        nome: 'flash',
        src: 'flash.png',
        alt: 'The Flash',
      },
      {
        id: 'spider',
        nome: 'spider',
        src: 'spider.png',
        alt: 'Homem Aranha',
      },
      {
        id: 'wolv',
        nome: 'wolv',
        src: 'wolv.png',
        alt: 'Wolverine',
      },
    ];
    this.imagemPadraoEscondido = 'ninja.png';
    this.heroisSelecionados = [];
  }

  inicializar() {
    this.tela.inserirCardsHtmlNaTela(this.imagensHeroisIniciais);
    this.tela.configurarBotaoJogar(this.jogar.bind(this));
    this.tela.configurarBotaoSelecionarCard(
      this.verificarSelecaoCard.bind(this)
    );
    this.tela.configurarBotaoMostrarTudo(
      this.mostrarHeroisEscondidos.bind(this)
    );
  }

  jogar() {
    this.embaralhar();
  }

  exibirHeroiCombinacaoCerta(nomeHeroi) {
    console.log('nomeHeroi' + nomeHeroi);
    const { src } = this.imagensHeroisIniciais.find(
      (heroi) => nomeHeroi === heroi.nome
    );
    this.tela.exibirHeroisCorretos(nomeHeroi, src);
  }

  async embaralhar() {
    const imagensHeroisDuplicadas = this.imagensHeroisIniciais
      .concat(this.imagensHeroisIniciais)
      .map((heroi) => {
        return Object.assign({}, heroi, { id: Math.random() / 0.5 });
      })
      .sort(() => Math.random() - 0.5);

    this.tela.inserirCardsHtmlNaTela(imagensHeroisDuplicadas);
    this.tela.exibirCarregando();

    const idDoIntervalo = this.tela.iniciarContador();

    await this.util.timeout(3000);
    this.tela.limparContador(idDoIntervalo);
    this.esconderCardsHerois(imagensHeroisDuplicadas);
    this.tela.exibirCarregando(false);
  }

  mostrarHeroisEscondidos() {
    const heroisEscondidos = this.cardsHeroisEscondidos;
    for (const heroi of heroisEscondidos) {
      const { src } = this.imagensHeroisIniciais.find(
        (item) => item.nome === heroi.nome
      );
      heroi.src = src;
    }
    this.tela.inserirCardsHtmlNaTela(heroisEscondidos);
  }

  esconderCardsHerois(imagensHeroisIniciais) {
    const cardsHeroisEscondidos = imagensHeroisIniciais.map(
      ({ id, alt, nome }) => ({
        id,
        alt,
        nome,
        src: this.imagemPadraoEscondido,
      })
    );

    this.cardsHeroisEscondidos = cardsHeroisEscondidos;
    this.tela.inserirCardsHtmlNaTela(cardsHeroisEscondidos);
  }

  verificarSelecaoCard(id, nome) {
    const heroi = { id, nome };
    const qtdheroisSelecionados = this.heroisSelecionados.length;

    switch (qtdheroisSelecionados) {
      case 0:
        this.heroisSelecionados.push(heroi);
        break;
      case 1:
        const [opcao1] = this.heroisSelecionados;
        this.heroisSelecionados = [];

        if (opcao1.nome === heroi.nome && opcao1.id !== heroi.id) {
          // alert('Acertou' + heroi.nome);
          this.tela.exibirMensagem();
          this.exibirHeroiCombinacaoCerta(heroi.nome);
          return;
        }

        this.tela.exibirMensagem(false);
        break;
    }
  }
}
