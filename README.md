<a>
    <img style="height: 103px; margin-top: -37px" src="./files/readme/lf.png" alt="Logo LF" title="logo" align="right" height="100" />
</a>

<br><br><br>

# Jogo da memória com JS

Jogo da memória simples aplicando vários fundamentos de fundamentos de JS. <br>
Este projeto é o desafio final do curso [Javascript para iniciante](http://conteudo.erickwendel.com.br/javascript-para-iniciantes) do [Erick Wendel](https://www.linkedin.com/in/erickwendel), que inclusive recomendo muito.

## Conteúdo
Foram abordados os seguindos itens:
- Variáveis, var, let, const
- Tipos de dados, Strings e Numbers
- Conhecendos os operadores do JS
- Diferença entre undefined, null
- booleanos, tipos
- Arrays, Objects
- Fluxo de dados com if, else, if else, switch case, While, Do While
- For, ForIn, ForOf
- Trabalhando com modulos no Node.js e Navegador
- Ciclo de vida do JS, callbacks, Promises, async/await
 

## Demo do game
![](files/readme/jogo.gif)